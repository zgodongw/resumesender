package com.cvsender.app.resources

import com.cvsender.app.models.ApplyRequest
import com.cvsender.app.models.GenericResponse
import com.cvsender.app.services.ApplyService
import org.springframework.http.ResponseEntity
import org.springframework.security.core.annotation.AuthenticationPrincipal
import org.springframework.security.oauth2.jwt.Jwt
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("apply")
class ApplyResource(val applyService: ApplyService) {

    @PostMapping()
    suspend fun apply(@RequestBody request: ApplyRequest, @AuthenticationPrincipal principal: Jwt): ResponseEntity<GenericResponse> {
        return applyService.apply(request, principal.subject)
    }

    @GetMapping()
    fun testApply(@AuthenticationPrincipal principal: Jwt): String {
        return principal.subject
    }
}