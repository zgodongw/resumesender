package com.cvsender.app.resources

import com.cvsender.app.models.*
import com.cvsender.app.services.CloudinaryService
import com.cvsender.app.services.ProfileService
import jakarta.validation.Valid
import org.keycloak.representations.AccessTokenResponse
import org.springframework.http.ResponseEntity
import org.springframework.http.codec.multipart.FilePart
import org.springframework.security.core.annotation.AuthenticationPrincipal
import org.springframework.security.oauth2.jwt.Jwt
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

@RestController
@RequestMapping("accounts")
class AccountsResource(val service: ProfileService, val cloudinaryService: CloudinaryService) {

    @PostMapping("register")
    suspend fun createAccount(@RequestBody @Valid request: CreateUserRequest): ResponseEntity<GenericResponse> {
        return service.createUser(request)
    }

    @PostMapping("login")
    suspend fun login(@RequestBody @Valid request: LoginRequest): ResponseEntity<AccessTokenResponse?> {
        return service.login(request)
    }


    @GetMapping("profile")
    suspend fun getProfile(@AuthenticationPrincipal principal: Jwt): UserExtended? {
        return service.getProfile(principal.subject)
    }

    @PutMapping("profile")
    suspend fun updateProfile(
        @RequestBody @Valid request: UpdateProfileRequest,
        @AuthenticationPrincipal principal: Jwt
    ): ResponseEntity<Profile> {
        return service.updateProfile(request, principal.subject)
    }

    @PostMapping("upload")
    suspend fun updateResume(
        @RequestPart file: FilePart,
        @AuthenticationPrincipal principal: Jwt
    ): Mono<ResponseEntity<GenericResponse>> {
        file.content()
        return cloudinaryService.upload(file, principal.subject)
    }
}