package com.cvsender.app.resources

import com.cvsender.app.models.Recent
import com.cvsender.app.services.RecentsService
import kotlinx.coroutines.flow.Flow
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.annotation.AuthenticationPrincipal
import org.springframework.security.oauth2.jwt.Jwt
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("recents")
class RecentsResource (@Autowired val recentsService: RecentsService){

    @GetMapping("")
    suspend fun getRecents(@AuthenticationPrincipal principal: Jwt): Flow<Recent> {
        return recentsService.getRecents(principal.subject)
    }
}