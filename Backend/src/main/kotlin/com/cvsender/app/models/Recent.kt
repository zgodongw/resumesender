package com.cvsender.app.models

import com.fasterxml.jackson.annotation.JsonProperty
import kotlinx.coroutines.flow.Flow
import org.springframework.data.annotation.Id
import org.springframework.data.r2dbc.repository.Query
import org.springframework.data.relational.core.mapping.Column
import org.springframework.data.relational.core.mapping.Table
import org.springframework.data.repository.kotlin.CoroutineCrudRepository

@Table("recent")
data class Recent(
    @Id val id: Long? = null,
    @Column("user_id") val userId: String,
    @Column("position") val position: String,
    @Column("created_at") @JsonProperty("created_at") val createdAt: String,
    @Column("email") val email: String,
    @Column("success") var success: Boolean
)


interface RecentRepository : CoroutineCrudRepository<Recent, Long> {

    @Query("SELECT * FROM recent WHERE user_id = :userId LIMIT 150")
    fun findAllByUserId(userId: String): Flow<Recent>
}