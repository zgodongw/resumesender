package com.cvsender.app.models

data class ApplyRequest(
    val position: String, val email: String
)
