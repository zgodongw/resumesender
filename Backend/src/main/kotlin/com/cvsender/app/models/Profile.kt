package com.cvsender.app.models

import jakarta.validation.constraints.Email
import jakarta.validation.constraints.NotBlank
import jakarta.validation.constraints.NotNull
import org.springframework.data.annotation.Id
import org.springframework.data.r2dbc.repository.Modifying
import org.springframework.data.r2dbc.repository.Query
import org.springframework.data.relational.core.mapping.Column
import org.springframework.data.relational.core.mapping.Table
import org.springframework.data.repository.kotlin.CoroutineCrudRepository
import org.springframework.transaction.annotation.Transactional
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import java.time.LocalDateTime

@Table("user_profile")
data class Profile(
    @Id val id: Long? = null,
    @Column("user_id") val userId: String,
    @Column("cv_url") val cvUrl: String?,
    @Column("phone") val phone: String?,
    @Column("base_message") val baseMessage: String?,
    @Column("created_at") val createdAt: String?
) {
    constructor(phone: String, userId: String) : this(
        id = null,
        userId,
        cvUrl = "",
        phone,
        baseMessage = "",
        createdAt = LocalDateTime.now().toString()
    )

    /**
     * Used only for upsert!
     * */
    constructor(phone: String? = "", userId: String, baseMessage: String? = "", cvUrl: String? = "") : this(
        id = 0,
        userId,
        cvUrl,
        phone,
        baseMessage,
        createdAt = LocalDateTime.now().toString()
    )
}

data class CreateUserRequest(
    val firstName: String,
    val lastName: String,
    @NotNull
    @NotBlank
    val password: String,
    val phone: String,
    @NotNull
    @NotBlank
    @Email
    val email: String
)

data class UpdateProfileRequest(
    val phone: String?,
    val baseMessage: String?,
    val cvUrl: String?
)

interface ProfileRepository : CoroutineCrudRepository<Profile, Long> {

    @Query("SELECT * FROM user_profile WHERE user_id = :userId LIMIT 1")
    suspend fun findByUserId(userId: String): Profile?

    @Modifying
    @Query(
        """
       
            UPDATE user_profile
                SET
                    phone = :phone,
                    base_message = :baseMessage,
                    cv_url = :cvUrl
                WHERE
                    user_id = :userId
                RETURNING *
        """
    )
    @Transactional
    suspend fun upsert(userId: String, phone: String, baseMessage: String, cvUrl: String): Flux<Profile>
}