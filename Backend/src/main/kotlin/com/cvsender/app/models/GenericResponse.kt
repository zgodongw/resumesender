package com.cvsender.app.models

data class GenericResponse(
    val success: Boolean, val message: String?
)
