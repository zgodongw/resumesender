package com.cvsender.app.models

import jakarta.validation.constraints.NotBlank
import jakarta.validation.constraints.NotNull

data class UserExtended(
    val id: String,
    val username: String,
    val firstName: String,
    val lastName: String,
    val email: String,
    val profile: Profile?
)

data class LoginRequest(
    @NotNull
    @NotBlank
    val username: String,
    @NotNull
    @NotBlank
    val password: String
)