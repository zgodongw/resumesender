package com.cvsender.app.services

import com.cvsender.app.models.GenericResponse
import com.cvsender.app.models.UserExtended
import jakarta.activation.URLDataSource
import jakarta.mail.internet.InternetAddress
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.mail.MailSendException
import org.springframework.mail.javamail.JavaMailSender
import org.springframework.mail.javamail.MimeMessageHelper
import org.springframework.stereotype.Service
import java.net.URL


@Service
class EmailService(@Autowired val mailSender: JavaMailSender) {

    suspend fun sendEmail(user: UserExtended?, to: String, position: String): GenericResponse {
        if (user?.profile == null) {
            return GenericResponse(success = false, message = "User has no profile.")
        }
        return try {
            val message = mailSender.createMimeMessage()
            val helper = MimeMessageHelper(message, true)
            helper.setFrom(
                InternetAddress("cv.sender@outlook.com", "${user.firstName} ${user.lastName}")
            )
            helper.setTo(to)
            helper.setSubject("CV for $position | ${user.firstName} ${user.lastName}")
            user.profile.baseMessage?.let { helper.setText(it) }
            user.profile.cvUrl?.let {
                helper.addAttachment("${user.firstName}.pdf", URLDataSource(URL(it)))
            }
            mailSender.send(message)

            GenericResponse(success = true, message = "Email Sent!")
        } catch (e: MailSendException) {
            GenericResponse(success = false, message = e.message)
        }
    }
}