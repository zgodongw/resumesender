package com.cvsender.app.services

import com.cloudinary.utils.ObjectUtils
import com.cvsender.app.config.CloudinaryConfig
import com.cvsender.app.models.GenericResponse
import com.cvsender.app.models.UpdateProfileRequest
import kotlinx.coroutines.reactor.mono
import org.springframework.core.io.buffer.DataBufferUtils
import org.springframework.http.ResponseEntity
import org.springframework.http.codec.multipart.FilePart
import org.springframework.stereotype.Service
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import java.io.File
import java.io.IOException

@Service
class CloudinaryService(val cloudinaryConfig: CloudinaryConfig, val profileService: ProfileService) {

    suspend fun upload(file: FilePart, userId: String): Mono<ResponseEntity<GenericResponse>> {
        val cloudinary = cloudinaryConfig.getCloud()

        val params = mapOf<String, Any>(
            "use_filename" to true,
            "unique_filename" to false,
            "overwrite" to true,
            "folder" to "cvsender/files",
            "public_id" to userId
        )

        return file.content()
            .map { it.asInputStream().readBytes() }
            .map { cloudinary.uploader().upload(it, params) }
            .flatMap {
                mono {
                    val uri = it["secure_url"] as String?
                    val upr = UpdateProfileRequest(null, null, uri)
                    profileService.updateProfile(upr, userId)
                }
            }
            .map { ResponseEntity.ok().body(GenericResponse(success = true, message = "Upload Success!")) }
            .next()
            .doOnError {
                ResponseEntity.badRequest().body(GenericResponse(success = false, message = it.message))
            }
    }
}