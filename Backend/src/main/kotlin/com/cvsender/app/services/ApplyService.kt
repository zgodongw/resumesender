package com.cvsender.app.services

import com.cvsender.app.models.*
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service
import java.time.LocalDateTime

@Service
class ApplyService(
    val repository: RecentRepository,
    val emailService: EmailService,
    val profileService: ProfileService
) {

    suspend fun apply(applyRequest: ApplyRequest, userId: String): ResponseEntity<GenericResponse> {

        val currentProfile = profileService.getProfile(userId)
        val result = emailService.sendEmail(currentProfile, to = applyRequest.email, position = applyRequest.position)
        val recent = Recent(
            userId = userId,
            position = applyRequest.position,
            createdAt = LocalDateTime.now().toString(),
            email = applyRequest.email,
            success = true
        )
        return if (result.success) {
            repository.save(recent);
            ResponseEntity.ok().body(result)
        } else {
            recent.success = false;
            repository.save(recent);
            ResponseEntity.badRequest().body(result)
        }
    }
}