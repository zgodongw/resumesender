package com.cvsender.app.services

import com.cvsender.app.models.Recent
import com.cvsender.app.models.RecentRepository
import kotlinx.coroutines.flow.Flow
import org.springframework.stereotype.Service

@Service
class RecentsService(val repository: RecentRepository) {

    fun getRecents(userId: String): Flow<Recent> {
        return repository.findAllByUserId(userId);
    }
}