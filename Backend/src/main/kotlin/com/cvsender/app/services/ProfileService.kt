package com.cvsender.app.services

import com.cvsender.app.config.KeycloakConfig
import com.cvsender.app.models.*
import jakarta.ws.rs.NotAuthorizedException
import jakarta.ws.rs.NotFoundException
import org.keycloak.admin.client.resource.UsersResource
import org.keycloak.representations.AccessTokenResponse
import org.keycloak.representations.idm.CredentialRepresentation
import org.keycloak.representations.idm.UserRepresentation
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service
import reactor.core.publisher.Mono


@Service
class ProfileService(val repository: ProfileRepository, val keycloackConfig: KeycloakConfig) {
    suspend fun updateProfile(updateProfile: UpdateProfileRequest, userId: String): ResponseEntity<Profile> {
        val currentProfile = repository.findByUserId(userId)
        val updatedProfile = checkForUpdate(currentProfile, updateProfile, userId)
        repository.upsert(
            updatedProfile.userId,
            updatedProfile.phone!!,
            updatedProfile.baseMessage!!,
            updatedProfile.cvUrl!!
        ).subscribe()
        return ResponseEntity.ok().body(updatedProfile)
    }

    suspend fun getProfile(userId: String): UserExtended? {
        val usersResource: UsersResource = keycloackConfig.getInstance().realm(keycloackConfig.realm).users()
        val userResource = usersResource.get(userId)
        val user = userResource.toRepresentation()
        val profile = repository.findByUserId(userId)

        return mapToUserExtended(user, profile)
    }

    suspend fun createUser(createUser: CreateUserRequest): ResponseEntity<GenericResponse> {
        val usersResource: UsersResource = keycloackConfig.getInstance().realm(keycloackConfig.realm).users()
        val userRepresentation = mapToUserRep(createUser)

        val response = usersResource.create(userRepresentation)

        if (response.status == 201) {
            val userId: String = response.location.path.toString().split("/").last()
            val profile = Profile(phone = createUser.phone, userId = userId)
            repository.save(profile)
            return ResponseEntity.ok()
                .body(GenericResponse(success = true, message = "Successfully created user: $userId"))
        }
        return ResponseEntity.badRequest().body(GenericResponse(success = false, message = "Failed to Create User"))
    }

    suspend fun login(loginRequest: LoginRequest): ResponseEntity<AccessTokenResponse?> {
        val kc = keycloackConfig.getPasswordTypeInstance(loginRequest.username, loginRequest.password)
        return try {
            val accessToken = kc.tokenManager().accessToken
            ResponseEntity.ok().body(accessToken)
        } catch (e: NotAuthorizedException) {
            val res = AccessTokenResponse()
            res.error = "Invalid Username otr Password."
            ResponseEntity.badRequest().body(res)
        } catch (e: NotFoundException) {
            val res = AccessTokenResponse()
            res.error = e.message
            ResponseEntity.badRequest().body(res)
        }

    }

    private fun createPasswordCredentials(password: String): CredentialRepresentation {
        val passwordCredentials = CredentialRepresentation()
        passwordCredentials.isTemporary = false
        passwordCredentials.type = CredentialRepresentation.PASSWORD
        passwordCredentials.value = password
        return passwordCredentials
    }

    private fun mapToUserRep(createUser: CreateUserRequest): UserRepresentation {
        val userRepresentation = UserRepresentation()
        val credentialRepresentation = createPasswordCredentials(createUser.password)

        userRepresentation.username = createUser.email
        userRepresentation.email = createUser.email
        userRepresentation.firstName = createUser.firstName
        userRepresentation.lastName = createUser.lastName
        userRepresentation.credentials = listOf(credentialRepresentation)
        userRepresentation.isEnabled = true
        userRepresentation.isEmailVerified = false

        return userRepresentation
    }

    private fun mapToUserExtended(userRepresentation: UserRepresentation, profile: Profile?): UserExtended {
        return UserExtended(
            id = userRepresentation.id,
            username = userRepresentation.email,
            firstName = userRepresentation.firstName,
            lastName = userRepresentation.lastName,
            email = userRepresentation.email,
            profile
        )
    }

    private fun checkForUpdate(currentProfile: Profile?, updateProfile: UpdateProfileRequest, userId: String): Profile {

        return currentProfile?.let {
            return Profile(
                userId = userId,
                phone = updateProfile.phone ?: it.phone,
                baseMessage = updateProfile.baseMessage ?: it.baseMessage,
                cvUrl = updateProfile.cvUrl ?: it.cvUrl,
            )
        } ?: return Profile(
            userId = userId,
            phone = updateProfile.phone,
            baseMessage = updateProfile.baseMessage,
            cvUrl = updateProfile.cvUrl
        )
    }
}