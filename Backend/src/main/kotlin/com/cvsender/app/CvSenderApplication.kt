package com.cvsender.app

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class CvSenderApplication

fun main(args: Array<String>) {
	runApplication<CvSenderApplication>(*args)
}
