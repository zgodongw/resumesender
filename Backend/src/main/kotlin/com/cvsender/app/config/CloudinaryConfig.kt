package com.cvsender.app.config

import com.cloudinary.*
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class CloudinaryConfig {
    @Value("\${cloudinary.url}")
    lateinit var url: String;

    @Bean
    fun getCloud(): Cloudinary {
        val cloudinary = Cloudinary(url)
        cloudinary.config.secure = true;
        return cloudinary;
    }
}