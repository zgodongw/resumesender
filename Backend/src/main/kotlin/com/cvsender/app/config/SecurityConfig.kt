package com.cvsender.app.config

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.HttpMethod
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity
import org.springframework.security.config.web.server.ServerHttpSecurity
import org.springframework.security.web.server.SecurityWebFilterChain
import org.springframework.security.web.server.context.NoOpServerSecurityContextRepository

@Configuration
@EnableWebFluxSecurity
class SecurityConfig {

    @Bean
    fun securityFilterChain(http: ServerHttpSecurity): SecurityWebFilterChain {
        http
            .csrf { it.disable() }
            .authorizeExchange {
                it
                    .pathMatchers("/accounts/register", "/accounts/login").permitAll()
                    .anyExchange().authenticated()
            }


        http
            .oauth2ResourceServer {
                it.jwt {}
            }

        http
            .securityContextRepository(NoOpServerSecurityContextRepository.getInstance())

        return http.build()
    }
}