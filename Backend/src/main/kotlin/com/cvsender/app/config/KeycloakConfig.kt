package com.cvsender.app.config

import jakarta.validation.constraints.NotBlank
import jakarta.validation.constraints.NotNull
import org.keycloak.OAuth2Constants
import org.keycloak.admin.client.Keycloak
import org.keycloak.admin.client.KeycloakBuilder
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class KeycloakConfig {
    @Value("\${keycloak.server}")
    lateinit var serverUrl: String;

    @Value("\${keycloak.realm}")
    lateinit var realm: String;

    @Value("\${keycloak.client-id}")
    lateinit var clientId: String;

    @Value("\${keycloak.credentials.secret}")
    lateinit var clientSecret: String;

    @Bean
    fun getInstance(): Keycloak {
        return KeycloakBuilder.builder()
            .realm(realm)
            .serverUrl(serverUrl)
            .clientId(clientId)
            .clientSecret(clientSecret)
            .grantType(OAuth2Constants.CLIENT_CREDENTIALS)
            .build();
    }

    fun getPasswordTypeInstance(username: String, password: String): Keycloak {
        return KeycloakBuilder.builder()
            .realm(realm)
            .serverUrl(serverUrl)
            .clientId(clientId)
            .clientSecret(clientSecret)
            .username(username)
            .password(password)
            .grantType(OAuth2Constants.PASSWORD)
            .build()
    }
}