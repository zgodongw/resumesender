CREATE TABLE IF NOT EXISTS recent (
    id serial PRIMARY KEY,
    user_id VARCHAR (255) NOT NULL,
    position VARCHAR (255) NOT NULL,
    created_at VARCHAR (255) NOT NULL
);