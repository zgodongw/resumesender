CREATE TABLE IF NOT EXISTS user_profile (
    id serial PRIMARY KEY,
    user_id VARCHAR (255) UNIQUE NOT NULL,
    cv_url VARCHAR (255) NOT NULL,
    phone VARCHAR (25) NOT NULL,
    base_message VARCHAR (1024) NOT NULL,
    created_at VARCHAR (255) NOT NULL
);