ALTER TABLE recent
ADD email varchar(255),
ADD success boolean NOT NULL default true;

--For Migration purposes
UPDATE recent
 SET email = 'example@ex.com';

ALTER TABLE recent
ALTER COLUMN email SET NOT NULL;