#!/bin/bash

# Warning: This file must run using npm from the same directory as your package.json

function createDirs() {
    echo "Creating $path_to_component/data-access/"
    mkdir "$path_to_component/data-access/"

    echo "Creating $path_to_component/data-access/services/"
    mkdir "$path_to_component/data-access/services/"

    echo "Creating $path_to_component/data-access/models/"
    mkdir "$path_to_component/data-access/models/"

    echo "Creating $path_to_component/data-access/stores/"
    mkdir "$path_to_component/data-access/stores/"

    echo "Creating $path_to_component/features/"
    mkdir "$path_to_component/features/"

    echo "Creating $path_to_component/ui/"
    mkdir "$path_to_component/ui/"

    echo "Creating $path_to_component/utils/"
    mkdir "$path_to_component/utils/"

    echo "Creating $path_to_component/utils/functions/"
    mkdir "$path_to_component/utils/functions/"

    echo "Creating $path_to_component/utils/guards/"
    mkdir "$path_to_component/utils/guards/"

    echo "Creating $path_to_component/utils/interceptors/"
    mkdir "$path_to_component/utils/interceptors/"
}


mod_name="$1"

if [ -z "$mod_name" ]
then
    echo "Invalid arguments. Only one positional argument required."
    exit 1
fi

path_to_component="./src/app/$mod_name"

if [ ! -d "$path_to_component" ] 
then
    echo "Warning: Directory $path_to_component/ does not exists."
    echo "Creating directory $path_to_component/"
    mkdir "$path_to_component/"
fi


createDirs

