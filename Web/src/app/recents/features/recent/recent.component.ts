import { Component } from '@angular/core';
import { RecentItem } from '../../data-access/models/recent.model';

@Component({
  selector: 'cv-recent',
  templateUrl: './recent.component.html',
  styleUrls: ['./recent.component.scss'],
})
export class RecentComponent {
  recents: RecentItem[] = [
    {
      position: 'Software Engineer',
      created_at: new Date(),
      email: 'example@test.com',
      success: true,
    },
    {
      position: 'Software Developer',
      created_at: new Date(),
      email: 'example@test.com',
      success: false,
    },
    {
      position: 'Senior Architect',
      created_at: new Date(),
      email: 'example@test.com',
      success: true,
    },
  ];
}
