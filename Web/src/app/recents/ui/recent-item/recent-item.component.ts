import { Component, Input } from '@angular/core';
import { RecentItem } from '../../data-access/models/recent.model';

@Component({
  selector: 'cv-recent-item',
  templateUrl: './recent-item.component.html',
  styleUrls: ['./recent-item.component.scss']
})
export class RecentItemComponent {
  @Input({required: true}) item!: RecentItem
}
