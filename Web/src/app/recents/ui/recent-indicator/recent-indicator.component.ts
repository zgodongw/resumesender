import { Component, Input } from '@angular/core';

@Component({
  selector: 'cv-recent-indicator',
  templateUrl: './recent-indicator.component.html',
  styleUrls: ['./recent-indicator.component.scss']
})
export class RecentIndicatorComponent {
  @Input() success = true
}
