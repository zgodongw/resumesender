export interface RecentItem{
    position: string;
    created_at: Date;
    email: string;
    success: boolean;
}