import { Component, OnDestroy, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { PageStore } from './shared/data-access/stores/page.store';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit, OnDestroy {
  title = 'cvsender';
  sub?: Subscription;

  constructor(private router: Router, private pageStore: PageStore) {}

  ngOnInit(): void {
    this.sub = this.router.events.subscribe((event: any) => {
      if (event instanceof NavigationEnd) {
        this.pageStore.set(this.router.url.substring(1));
      }
    });
  }
  ngOnDestroy(): void {
    this.sub?.unsubscribe();
  }
}
