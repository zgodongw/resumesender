import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './auth/features/login/login.component';
import { RegisterComponent } from './auth/features/register/register.component';
import { ApplyComponent } from './apply/features/apply/apply.component';
import { ProfileComponent } from './profile/features/profile/profile.component';
import { RecentComponent } from './recents/features/recent/recent.component';
import { LayoutComponent } from './shared/features/layout/layout.component';
import { NotFoundComponent } from './shared/features/not-found/not-found.component';

const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent,
  },
  {
    path: 'register',
    component: RegisterComponent,
  },
  {
    path: '',
    component: LayoutComponent,
    children: [
      { path: 'apply', component: ApplyComponent },
      { path: '', redirectTo: '/apply', pathMatch: 'full' },
      { path: 'profile', component: ProfileComponent },
      { path: 'recents', component: RecentComponent }
    ],
  },
  { path: '404', component: NotFoundComponent },
  { path: '**', redirectTo: '/404' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
