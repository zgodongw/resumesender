export interface AccessTokenDto {
    access_token:        string;
    expires_in:          number;
    refresh_expires_in:  number;
    refresh_token:       string;
    token_type:          string;
    id_token:            string | null;
    "not-before-policy": number;
    session_state:       string;
    scope:               string;
    error:               string | null;
    error_description:   string | null;
    error_uri:           string | null;
}
