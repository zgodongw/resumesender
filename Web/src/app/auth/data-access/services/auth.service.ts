import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { LoginDto } from '../models/Login.model';
import { observe } from 'src/app/shared/utils/functions/observe';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor(private http: HttpClient) {}

  auth_key = '6Z8I-XPRE';

  login(form: LoginDto) {
    return observe(this.http.post<LoginDto>("/accounts/login", form))
  }

  setAuthToken(str: string) {
    localStorage.setItem(this.auth_key, str);
  }

  getAuthToken(str: string) {
    localStorage.getItem(this.auth_key)
  }
}
