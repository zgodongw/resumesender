import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'cv-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {

  loginForm = new FormGroup({
    username: new FormControl('', Validators.required),
    password: new FormControl('', Validators.required)
  })

  get username() {
    return this.loginForm.get('username')
  }

  get password() {
    return this.loginForm.get('password')
  }

  get invalidUsername() {
    if (this.username?.invalid && (this.username?.dirty || this.username?.touched)) {
      return true;
    }
    return undefined
  }

  get invalidPassword() {
    if (this.password?.invalid && (this.password?.dirty || this.password?.touched)) {
      return true;
    }
    return undefined
  }

  

}
