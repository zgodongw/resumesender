import { Component } from '@angular/core';

@Component({
  selector: 'cv-apply',
  templateUrl: './apply.component.html',
  styleUrls: ['./apply.component.scss']
})
export class ApplyComponent {

  line = "Hi\n\nhello\ngoodby";
  line_count = this.line.split(/\r\n|\r|\n/).length
}
