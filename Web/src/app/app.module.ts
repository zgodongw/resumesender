import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './auth/features/login/login.component';
import { RegisterComponent } from './auth/features/register/register.component';
import { ApplyComponent } from './apply/features/apply/apply.component';
import { ProfileComponent } from './profile/features/profile/profile.component';
import { RecentComponent } from './recents/features/recent/recent.component';
import { LayoutComponent } from './shared/features/layout/layout.component';
import { NotFoundComponent } from './shared/features/not-found/not-found.component';
import { NavUnitComponent } from './shared/ui/nav-unit/nav-unit.component';
import { GoodluckComponent } from './shared/ui/goodluck/goodluck.component';
import { RecentItemComponent } from './recents/ui/recent-item/recent-item.component';
import { RecentIndicatorComponent } from './recents/ui/recent-indicator/recent-indicator.component';
import { SpinnerComponent } from './shared/ui/spinner/spinner.component';
import { ReactiveFormsModule } from '@angular/forms';
import { FileUploaderComponent } from './shared/features/file-uploader/file-uploader.component';
import { ProgressLoaderComponent } from './shared/ui/progress-loader/progress-loader.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    ApplyComponent,
    ProfileComponent,
    RecentComponent,
    LayoutComponent,
    NotFoundComponent,
    NavUnitComponent,
    GoodluckComponent,
    RecentItemComponent,
    RecentIndicatorComponent,
    SpinnerComponent,
    FileUploaderComponent,
    ProgressLoaderComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
