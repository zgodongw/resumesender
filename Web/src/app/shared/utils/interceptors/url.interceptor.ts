import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable()
export class UrlInterceptor implements HttpInterceptor {
  
  constructor() {}

  private skippable(url: string): boolean {
    const skippableUrls = environment.externalAPIs; //Add Urls to skip authentication check
    for (const val of skippableUrls) {
      if (url.includes(val)) {
        return true;
      }
    }
    return false;
  }

  intercept(
    httpRequest: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    const url = httpRequest.url;
    if (
      this.skippable(httpRequest.url) ||
      url.includes(environment.apiBaseUrl)
    ) {
      return next.handle(httpRequest);
    }
    return next.handle(
      httpRequest.clone({ url: environment.apiBaseUrl + url })
    );
  }
}
