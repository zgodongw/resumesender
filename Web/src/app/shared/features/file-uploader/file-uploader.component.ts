import { Component, ElementRef, Input, ViewChild } from '@angular/core';


@Component({
  selector: 'cv-file-uploader',
  templateUrl: './file-uploader.component.html',
  styleUrls: ['./file-uploader.component.scss']
})
export class FileUploaderComponent {

  @ViewChild('fileInput') fileInput?: ElementRef;

  @Input() filename = ""

  loading = false;
  
  onUploadClick() {
    let event = new MouseEvent('click', { bubbles: true });
    this.fileInput?.nativeElement.dispatchEvent(event);
  }

  onFileSelected(event: Event) {
    const target = event.target as HTMLInputElement;
    if (target.files && target.files[0]) {
      this.loading = true
      const file = target.files[0];
      this.filename = file.name

      setTimeout(() => {
        this.loading = false
      }, 2000);
    }
  }
}
