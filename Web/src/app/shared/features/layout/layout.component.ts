import { Component, OnDestroy, OnInit } from '@angular/core';
import { PageStore } from '../../data-access/stores/page.store';
import { PAGES } from '../../data-access/data/pages';
import { Page } from '../../data-access/models/page.model';
import { Subscription } from 'rxjs';
import { NavigationEnd, Router } from '@angular/router';

@Component({
  selector: 'cv-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss'],
})
export class LayoutComponent implements OnInit, OnDestroy {
  pages = PAGES;
  sub?: Subscription;

  currentPage?: Page;

  constructor(private pageStore: PageStore, private router: Router) {}
  
  get isHomePage() {
    return this.currentPage?.uri === 'apply';
  }

  ngOnInit(): void {
    this.sub = this.pageStore.get().subscribe((value) => {
      this.currentPage = this.pages.filter((page) => page.uri === value)[0];
    });
  }

  ngOnDestroy(): void {
    this.sub?.unsubscribe();
  }

  navigate(page: string) {
    this.router.navigate([page]);
  }
}
