import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PageStore {
  /*
   * @Description PageStore to behold state for the current active page on route
   */
  private store = new BehaviorSubject<string>("");
  private state$ = this.store.asObservable();

  constructor() { }

  set(page: string) {
    this.store.next(page);
  }

  get() {
    return this.state$
  }

  getLastestSnapshot() {
    return this.store.getValue()
  }
}
