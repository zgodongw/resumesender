import { Page } from "../models/page.model";

export const PAGES: Page[] = [
    {
        uri: "apply",
        name: "Apply",
        active: false,
        icon: "hands"
    },
    {
        uri: "recents",
        name: "Recents",
        active: false,
        icon: "nail"
    },
    {
        uri: "profile",
        name: "Profile",
        active: false,
        icon: "boy"
    }
]