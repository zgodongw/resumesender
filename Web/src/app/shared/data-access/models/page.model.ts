export interface Page {
    uri: string;
    name: string;
    active: boolean;
    icon: 'fire' | 'hands' | 'nail' | 'boy';
}