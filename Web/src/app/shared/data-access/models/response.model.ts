export interface IResponse<T> {
  results?: T;
  errors?: string[];
}