import { Component, Input } from '@angular/core';

@Component({
  selector: 'cv-nav-unit',
  templateUrl: './nav-unit.component.html',
  styleUrls: ['./nav-unit.component.scss'],
})
export class NavUnitComponent {
  @Input() active = false;
  @Input() icon: 'fire' | 'hands' | 'nail' | 'boy' = 'fire';
  @Input({ required: true }) title = '';

  get display_icon() {
    if (this.active) {
      return 'assets/emoji/icons8-fire-48.png';
    }
    switch (this.icon) {
      case 'fire':
        return 'assets/emoji/icons8-fire-48.png';
      case 'hands':
        return 'assets/emoji/icons8-folded-hands-emoji-48.png';
      case 'nail':
        return 'assets/emoji/icons8-nail-polish-48.png';
      case 'boy':
        return 'assets/emoji/icons8-boy-48.png';
      default:
        return 'assets/emoji/icons8-folded-hands-emoji-48.png';
    }
  }
}
